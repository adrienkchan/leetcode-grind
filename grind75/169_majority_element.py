class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        mode_dict = {}
        count = 0
        mode = None
        for n in nums:
            if n in mode_dict:
                mode_dict[n] += 1
            else:
                mode_dict[n] = 1
            if mode_dict[n] > count:
                 count = mode_dict[n]
                 mode = n
        return mode
